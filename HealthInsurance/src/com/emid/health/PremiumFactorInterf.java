package com.emid.health;

public interface PremiumFactorInterf {
	
	
	
	public double checkAge();
	public int basicPremium();
	public double genderRule();
	public double preCondition();
	public double goodHealth();
	public double badHealth();
	public double calPremium();
	

}
