package com.emid.health;

//PraveenB 
//Output: Premium for Norman Gomes with Age:34 is :6500.09

public class Premium extends Insurance implements PremiumFactorInterf{

 
	double ageFactor;
	
	String name;
	String gender;
	int age;
	
	
	public Premium(String name, String gender, int age) {
		// TODO Auto-generated constructor stub
		super(name, gender, age);
		this.name=name;
		this.gender=gender;
		this.age=age;
		
	}

	@Override
	public double checkAge() {
		// TODO Auto-generated method stub
		double premium;
		if(this.age<18)
			 premium= basicPremium();
		else
			if(this.age>=18  && this.age<=25)
			{
				premium=basicPremium()+(basicPremium()*0.10);
			}
			else if(this.age>25  && this.age<=30)
			{
				premium= basicPremium()+(basicPremium()*0.20);
			}
			else if(this.age>30  && this.age<=35)
			{
				premium= basicPremium()+(basicPremium()*0.30);
			}
			else if(this.age>35  && this.age<=40)
			{
				premium= basicPremium()+(basicPremium()*0.40);
			}
			else
				premium= basicPremium()+(basicPremium()*0.60);
		return premium;
		
		
	}

	@Override
	public int basicPremium() {
		// TODO Auto-generated method stub
		return 5000;
	}

	@Override
	public double genderRule() {
		// TODO Auto-generated method stub
		if(gender.equals("Male"))
		return 0.02;
		else
			return 1;
	}

	@Override
	public double preCondition() {
		// TODO Auto-generated method stub
		return this.getPreExistCondition()*0.01;
		
	}

	@Override
	public double goodHealth() {
		// TODO Auto-generated method stub
		
		return -(this.getGoodHabitCount()*0.03);
		
	}

	@Override
	public double badHealth() {
		// TODO Auto-generated method stub
		return (this.getBadHabitCount()*0.03);
	}

	@Override
	public double calPremium() {
		// TODO Auto-generated method stub
		return this.checkAge()+this.genderRule()+this.preCondition()-this.goodHealth()+this.badHealth();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Premium one = new Premium("Ram", "Male", 34);
		one.setCurrentHealth("Overweight", 1);
		one.setHabbit("Alcohol", 1);
		one.setHabbit("Daily_exercise", 1);
	
		System.out.print("Premium for Norman Gomes with Age:34 is :");
		System.out.println(one.calPremium());
		
				
		
		
		
		

	}
	
	

}
